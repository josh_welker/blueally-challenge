# Setting Up Your Environment
To make things easier for you, we have provided a VM using Vagrant to get you up and going so you can focus on writing your code. Please follow the steps below.

### Local Hosts File
1. Edit hosts file on local machine `sudo nano /etc/hosts`
2. Add entry `192.168.56.101  blueallychallenge.local`
3. Save `crtl + O`, press `enter` when prompted
4. Exit `ctrl + X`

### Installing Vagrant
1. You will need to install [Vagrant](https://www.vagrantup.com/) and VM software on your machine if you do not already. We recommend [VirtualBox](https://www.virtualbox.org/).
2. Unzip the blueally-challenge file given to you, (typically in your "Sites" directory), where you keep your local development projects.
3. Inside the new blueally-challenge directory run the following command: `composer create-project symfony/skeleton api` This will set up a new Symfony project named, you guessed it, "api". This directory will be shared on the Vagrant machine to /var/www.
	- Once you start developing, you should use the following database configuration:
	    - name: `dbname`, user: `dbuser`, password: `123`
3. Provision Vagrant `vagrant up`. This will take a few minutes, grab a cup of coffee, go for a walk, do some push ups in the mean time. ;)
	- You may get an error when it completes, `The SSH command responded with a non-zero exit status.` You can ignore that and continue onto the next steps.
5. SSH into the VM `vagrant ssh`.
6. Navigate to application root `cd /var/www`.
    - Run this command: `sudo chmod -R 777 var/cache && sudo rm -R html/ && bin/console cache:clear`
	- Enter `ls` to list all the folders inside.
	- You should see `bin/  cgi-bin/  composer.json*  composer.lock*  config/  public/  src/  symfony.lock*  var/  vendor/`
7. Now you should be able to load [blueallychallenge.local](http://blueallychallenge.local) in your browser and see a welcome to Symfony landing page!

### The Code Challenge

###### Description:
  - Create an Angular 7+ application that lists IP addresses, organized by subnet, using CIDR notation. When a user clicks on a subnet, its IP addresses should expand beneath it. When the user clicks on one of these IP addresses, its detail information should display next to the list.
  - Using Symfony 4+ and OOP principles, create a simple REST API endpoint(s) which is consumed by the Angular application detailed above.
  - Please use the included JSON file named "subnets.json" as a basis for the database structure that houses the data returned by your API endpoint(s).

###### Design/Layout:
  - Use the diagram included in this folder, named "layout-diagram.jpg," as a reference. Your layout and design does not need to match it exactly. You can use any extra Angular modules as you see fit (Bootstrap, PrimeNG, etc.).
      
###### Delivery:
  - You can put the final project on GitHub, GitLab, or Bitbucket and send a link for download.
      
###### Questions: 
  - As in real projects, this challenge has ambiguities and no single right answer. If you have any questions pertaining to this challenge, please feel free to contact me at any time.

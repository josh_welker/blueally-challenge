# BlueAlly Challenge
Thank you all for taking time to review my work here. Fingers crossed that nothing explodes.

Here are some steps to make sure it works properly.

## Symfony
SSH into Vagrant and run these:
```
cd /var/www
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```

## Angular
I am running Angular from a command line _outside_ of Vagrant:
```
cd [whatever]/blueally-challenge/frontend
ng serve
```
The Angular app will run on [localhost:4200](http://localhost:4200), but it assumes the Symfony API is running on [blueallychallenge.local](http://blueallychallenge.local).

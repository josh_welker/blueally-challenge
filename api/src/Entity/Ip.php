<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;


/**
 * @ORM\Entity(repositoryClass="App\Repository\IpRepository")
 */
class Ip
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tag;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subnet", inversedBy="ips")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $subnet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    public function getSubnet(): ?Subnet
    {
        return $this->subnet;
    }

    public function setSubnet(?Subnet $subnet): self
    {
        $this->subnet = $subnet;

        return $this;
    }


    // This calculates the CIDR number for this address based on
    // the number of IP addresses belonging to the parent subnet
    public function getCidrNumber(): ?int
    {
      // First determine the number of IPs in the subnet
      $ipCount = count($this->getSubnet()->getIps());

      // Divide the number of IPs by 2 until it can't be divided anymore. This will give
      // us a count of significant bits
      $bitTracker = $ipCount;
      $bitCount = 0;
      while( $bitTracker > 1 ){
        $bitTracker = $bitTracker / 2;
        $bitCount = $bitCount + 1;
      }

      // Subtract significant bits from 32
      $cidrNumber = 32 - $bitCount;

      return $cidrNumber;
    }

    // This is the string that gets serialized and sent via API
    public function getCidrNotation(): ?string
    {
      return $this->getAddress() . '/' . $this->getCidrNumber();
    }
}

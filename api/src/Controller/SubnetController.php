<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Subnet;


class SubnetController extends AbstractController
{
  /**
   * @Route("/subnets", name="subnets")
   */
  public function index(SerializerInterface $serializer)
  {
    $subnets = $this->getDoctrine()->getRepository(Subnet::class)->findAll();
    $serializedSubnets = $serializer->serialize($subnets, 'json', [
      'circular_reference_handler' => function ($object) {
        return $object->getId();
      }
    ]);
    $response = new Response($serializedSubnets);
    $response->headers->set('Access-Control-Allow-Origin', '*'); #This is not super secure, and ideally we'd whitelist specific domains in an app config file
    return $response;
  }
}

import { TestBed } from '@angular/core/testing';

import { SubnetsService } from './subnets.service';

describe('SubnetsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubnetsService = TestBed.get(SubnetsService);
    expect(service).toBeTruthy();
  });
});

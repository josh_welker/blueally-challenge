import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubnetsService {

  private subnets;
  subnetsUrl = 'http://blueallychallenge.local/index.php/subnets';

  constructor(private http: HttpClient) {
    this.http = http;
    this.subnets = this.fetchSubnets();
  }

  getSubnets(){
    return this.subnets;
  }

  fetchSubnets(){
    return this.http.get(this.subnetsUrl, {observe: 'response'});
  }
}

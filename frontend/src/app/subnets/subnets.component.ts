import { Component, OnInit } from '@angular/core';
import { SubnetsService } from './subnets.service';

@Component({
  selector: 'app-subnets',
  templateUrl: './subnets.component.html',
  styleUrls: ['./subnets.component.scss']
})
export class SubnetsComponent implements OnInit {

  private subnets;
  private currentSubnet;
  private currentIp;

  constructor(private subnetsService: SubnetsService) {
    this.subnetsService.getSubnets().subscribe(response => {
      this.subnets = response.body;
    });
  }

  ngOnInit() {
  }

  selectIp(ip, subnet){
    this.currentSubnet = subnet;
    this.currentIp = ip;
  }

}

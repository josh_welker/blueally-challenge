import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubnetsComponent } from './subnets/subnets.component';

const routes: Routes = [
  { path: '', component: SubnetsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
